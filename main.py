# This is the initial attempt to connect to the Ambient Weather API and pull data from Lake NW sensor

from ambient_api.ambientapi import AmbientAPI
import datetime
import os
import asyncio
import logging
from aioambient import Websocket
from aioambient.errors import WebsocketError

macaddress_name = []


def print_data(data):
    """Print data as it is received."""
    print('At ' + str(datetime.datetime.fromtimestamp(data.get('dateutc') / 1000)))
    sensor_name = [x for x in macaddress_name if data.get('macAddress') in x]
    print('Sensor Name: ' + sensor_name[0][1])
    print('Wind Speed: ' + str(data.get('windspeedmph')))
    print('Wind Direction: ' + str(data.get('winddir')))

def print_goodbye():
    """Print a simple "goodbye" message."""
    print("Client has disconnected from the websocket")


def print_hello():
    """Print a simple "hello" message."""
    print("Client has connected to the websocket")


def print_subscribed(data):
    """Print subscription data as it is received."""

    for device in data.get('devices'):
        print('At ' + str(datetime.datetime.fromtimestamp(device.get('lastData').get('dateutc') / 1000)))
        print('Sensor Name: ' + device.get('info').get('name'))
        print('Wind Speed: ' + str(device.get('lastData').get('windspeedmph')))
        print('Wind Direction: ' + str(device.get('lastData').get('winddir')))
        macaddress_name.append([device.get('macAddress'), device.get('info').get('name')])



async def main() -> None:
    """Run the websocket example."""
    websocket = Websocket(app_key, api_key)
    websocket.on_connect(print_hello)
    websocket.on_disconnect(print_goodbye)
    websocket.on_subscribed(print_subscribed)
    websocket.on_data(print_data)

    try:
        await websocket.connect()
    except WebsocketError as err:
        print("There was a websocket error: %s", err)
        return

    while True:
        #print("Simulating some other task occurring...")
        await asyncio.sleep(5)

if __name__ == '__main__':
    # initialize variables from environment
    try:
        endpoint = os.environ["AMBIENT_ENDPOINT"]
        rt_endpoint = os.environ["AMBIENT_RT_ENDPOINT"]
        app_key = os.environ["AMBIENT_APPLICATION_KEY"]
        api_key = os.environ["AMBIENT_API_KEY"]
    except KeyError:
        pass

    # Loop through the devices and get the name, wind speed, and wind direction.
    api = AmbientAPI()
    devices = api.get_devices()
    for device in devices:
        print('At ' + str(datetime.datetime.fromtimestamp(device.last_data.get('dateutc')/1000)))
        print('Sensor Name: ' + device.info.get('name'))
        print('Wind Speed: ' + str(device.last_data.get('windspeedmph')))
        print('Wind Direction: ' + str(device.last_data.get('winddir')))

    # Connect to the Real Time API and pull continuous data
    asyncio.run(main())

